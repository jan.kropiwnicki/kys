# kys

# PL

## O komendzie

Komenda kys wyświetla w terminalu wysoki na 5 linii napis "KYS"

## Instalacja

Po pobraniu repozytorium należy uruchomić skrypt install-kys.sh:

```
./install-kys.sh
```

Jeżeli plik się nie uruchamia, należy użyć komendy:

```
chmod +x install-kys.sh
```

## Użycie

Wystarczy w terminalu wpisać komendę
```
kys
```

# EN

## About command

The kys command displays a 5-line-tall text reading "KYS" in the terminal.

## Installation

After downloading the repository, you need to run the install-kys.sh script.

```
./install-kys.sh
```

If the script does not run, use this command:

```
chmod +x install-kys.sh
```

## Usage

You just need to invoke the command in the terminal.
```
kys
```
